## 1.0.14

* Update package.json
* Update .npmignore

## 1.0.13

* Fix 1 bug - FastjsArray run hooks

## 1.0.12

* Update FastjsArray - add function addHooks()

## 1.0.11

* Fix 1 bug - FastjsArray add element place error

## 1.0.10

* Fix 1 bug - FastjsArray remove element will cause index error and can't remove element correctly
* Update .npmignore
* Update package.json

## 1.0.9

* Update README.md

## 1.0.8

* Update README.md

## 1.0.7

* Fix 1 bug - FastjsArray.add

## 1.0.6

* Fix 1 bug - type check in FastjsArray

## 1.0.5

* Remove log output

## 1.0.4

* Fix 1 bug - type check error in FastjsArray

## 1.0.3

* Fix 1 bug - uncorrected entry in package.json

## 1.0.2

* Optimized type checking

## 1.0.1

* Update package detail