# Description

::: tip
This docs is for fastjs-next, if you want to see the docs for fastjs, please visit [here](https://fastjs.com.cn/fastjs-docs/).
:::

## What is it?

Fastjs is a powerful, fast and small JavaScript Frame. It is designed to be easy to use, and it is very easy to learn. Fastjs can use with many frame, like vue, react, angular.

Fastjs is a good choice for you to develop your project, Our team are working hard to make it better.

## Why Fastjs?

We are trying to make a small, fast and powerful JavaScript Frame. Fastjs can use in a lot of project, like vue, react, angular. Fastjs is a good choice for you to develop your project.

## Developer

Fastjs is developed by [Fastjs Team](https://github.com/fastjs-team).

- [dy-xiaodong2022](https://xiaodong.indouyin.cn/)
- shugoforgit